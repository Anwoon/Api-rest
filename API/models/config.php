<?php

    $config = [
        'database' => [
            'user' => 'root',
            'password' => 'root',
            'host' => 'localhost' ,
            'database' => 'meetup',
        ],
        'prefix' => 'API/',
    ];

    define('CONFIG', $config);

?>