<?php
namespace Models;

class Login extends Model{

    function __construct(){
        parent::__construct();
    }

    function getLogin($ndc,$mdp){
        $request = $this->getConnection()->prepare("SELECT * FROM subscriber WHERE first_name =:ndc AND password=:mdp");
        $request->execute([
            'ndc'=>$ndc,
            'mdp'=>$mdp
        ]);
        return $request->fetch();
      }
}
?>