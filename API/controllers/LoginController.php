<?php

namespace Controllers;
use \Firebase\JWT\JWT;
use Models\Login;

class LoginController extends Controller {
    public function login() {
        if (isset($_POST['mdp']) && isset($_POST['ndc'])) {
            $psw = $_POST['mdp'];
            $ndc = $_POST['ndc'];
            $login = new Login();
            $getlogin = $login->getLogin($ndc,$psw);
            if($getlogin['password'] != $psw || $getlogin['first_name'] != $ndc){
                echo json_encode(['login' => 'Nom de compte ou mot de passe incorrecte']);
                //http_response_code(401);
            }else{
            $key = "n_ebw/+LhJ{8w^j&h?+E*&x";
                $token = array(
                    "ndc" => $ndc,
                    "id_user"=>$getlogin['id'],
                    "exp" => time() + 2*24*60*60,
                    "iat" => time()
                );

                $jwt = JWT::encode($token, $key);
                echo json_encode(['login' => 'Vous ete bien connecté', 'token' => $jwt]);
            }

        }
    }
    public function error() {
        echo json_encode(['login' => 'Vous devez vous connectée pour effectuée cette action']);
    }
}